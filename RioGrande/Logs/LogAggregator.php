<?php

/**
 * Abstract base for log data aggregation.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage Logs
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-logparser
 * @since      0.1.0
 */

namespace RioGrande\Logs;

abstract class LogAggregator
{
    abstract public function processData(LogLine $Line, array $Data): void;
    abstract public function getData(): array;
    abstract public function getJSON(): string;
    abstract public function getCSV(): string;
    abstract public function getLatexTable(): string;

    public function getFormattedData(string $AFormat): string
    {
        switch ($AFormat) {
            case 'json':
                return $this->getJSON();
                break;
            case 'csv':
                return $this->getCSV();
                break;
            case 'latex':
                return $this->getLatexTable();
                break;
        }
    }
}
