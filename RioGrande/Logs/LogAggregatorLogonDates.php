<?php

/**
 * Log data aggregation for logon dates.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage Logs
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-logparser
 * @since      0.1.0
 */

namespace RioGrande\Logs;

class LogAggregatorLogonDates extends LogAggregator
{
    private array $Counts = [];

    public function processData(LogLine $Line, array $Data): void
    {
        if (0 == strlen($Data['username'])) {
            return;
        }
        if (!isset($this->Counts[$Data['date']])) {
            $this->Counts[$Data['date']] = array();
        }
        $a = $this->Counts[$Data['date']];
        if (!isset($a[$Data['username']])) {
            $a[$Data['username']] = 0;
        }
        $a[$Data['username']] = $a[$Data['username']] + 1;
        $this->Counts[$Data['date']] = $a;
    }

    public function getData(): array
    {
        $aData = $this->Counts;
        ksort($aData);
        return $aData;
    }

    public function getJSON(): string
    {
        $aData = $this->getData();
        $sRet = "[\n";
        foreach ($aData as $sDate => $aDetails) {
            var_dump($aDetails);
            $sCounts = '';
            foreach ($aDetails as $sUsername => $iCount) {
                $sUserComma = ($sUsername === array_key_last($aDetails)) ? '' : ',';
                $sCounts .= "      { \"username\": \"{$sUsername}\", \"count\": {$iCount} }{$sUserComma}\n";
            }
            $sComma = ($sDate === array_key_last($aData)) ? '' : ',';
            $sRet .= "  {\n    \"date\": \"{$sDate}\",\n    \"counts\": [\n{$sCounts}    ]\n  }{$sComma}\n";
        }
        $sRet .= "]\n";
        return $sRet;
    }

    public function getCSV(): string
    {
        $aData = $this->getData();
        $sRet = "date,username,count\n";
        foreach ($aData as $sDate => $aDetails) {
            foreach ($aDetails as $sUsername => $iCount) {
                $sRet .= "{$sDate},{$sUsername},{$iCount}\n";
            }
        }
        return $sRet;
    }

    public function getLatexTable(): string
    {
        $aData = $this->getData();
        $sRet = <<<FOOBAR

        
        \begin{table}
        \begin{tabular}[h]{l|r}
        Datum & Benutzer und Häufigkeiten \\\\
        \\hline \\\\

        FOOBAR;
        foreach ($aData as $sDate => $aDetails) {
            $sMore = 'bb';
            $sRet .= "{$sDate} & {$sMore} \\\\\n";
        }
        $sRet .= <<<FOOBAR
        \\end{tabular}
        \caption{Quod erat demonstrandum.}
        \label{table:xxxxx}
        \\end{table}


        FOOBAR;
        return $sRet;
    }
}
