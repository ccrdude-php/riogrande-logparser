<?php

namespace RioGrande\Logs;

abstract class LogLine
{
    abstract public function hasResults(): bool;
    abstract public function getDate(): string;
    abstract public function getTime(): string;
    abstract public function getIP(): string;
    abstract public function getStatusCode(): string;
    abstract public function getTimeCode(): string;
    abstract public function getURL(): string;
    abstract public function getQuery(): string;
    abstract public function getCookie(): string;
}
