<?php

/**
 * Log data aggregation for logon counts.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage Logs
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-logparser
 * @since      0.1.0
 */

namespace RioGrande\Logs;

class LogAggregatorLogonCounts extends LogAggregator
{
    private array $Counts = [];

    public function processData(LogLine $Line, array $Data): void
    {
        if (0 == strlen($Data['username'])) {
            return;
        }
        if (!isset($this->Counts[$Data['username']])) {
            $this->Counts[$Data['username']] = 0;
        }
        $this->Counts[$Data['username']] = $this->Counts[$Data['username']] + 1;
    }

    public function getData(): array
    {
        $aData = $this->Counts;
        ksort($aData);
        return $aData;
    }

    public function getJSON(): string
    {
        $aData = $this->getData();
        $sRet = "[\n";
        foreach ($aData as $sUsername => $iCount) {
            $sComma = ($sUsername === array_key_last($aData)) ? '' : ',';
            $sRet .= "  { \"username\": \"{$sUsername}\", \"count\": {$iCount} }{$sComma}\n";
        }
        $sRet .= "]\n";
        return $sRet;
    }

    public function getCSV(): string
    {
        $aData = $this->getData();
        $sRet = "username,count\n";
        foreach ($aData as $sUsername => $iCount) {
            $sRet .= "{$sUsername},{$iCount}\n";
        }
        return $sRet;
    }

    public function getLatexTable(): string
    {
        $aData = $this->getData();
        $sRet = <<<FOOBAR

        
        \begin{table}
        \begin{tabular}[h]{l|r}
        Benutzername & Häufigkeit \\\\
        \\hline \\\\

        FOOBAR;
        foreach ($aData as $sUsername => $iCount) {
            $sRet .= "{$sUsername} & {$iCount} \\\\\n";
        }
        $sRet .= <<<FOOBAR
        \\end{tabular}
        \caption{Quod erat demonstrandum.}
        \label{table:xxxxx}
        \\end{table}


        FOOBAR;
        return $sRet;
    }
}
