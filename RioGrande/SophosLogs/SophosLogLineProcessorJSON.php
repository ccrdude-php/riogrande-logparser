<?php

/**
 * Processes data from a single log line into JSON output.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage SophosLogs
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-logparser
 * @since      0.1.0
 */

namespace RioGrande\SophosLogs;

class SophosLogLineProcessorJSON extends SophosLogLineProcessor
{
    public function getFilePrefix(): string
    {
        return "{\n";
    }

    public function getFileSuffix(): string
    {
        return "}\n";
    }

    public function getTextFromLine(SophosLogLine $ALine): string
    {
        $aData = $this->getDataArray($ALine);
        $sRet = "  {\n";
        foreach ($aData as $sKey => $sValue) {
            $sRet .= "    \"{$sKey}\": \"{$sValue}\",\n";
        }
        $sRet .= "  },\n";
        return $sRet;
    }
}
