<?php

/**
 * Handles a pool of Sophos Firewall log files.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage SophosLogs
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-logparser
 * @since      0.1.0
 */

namespace RioGrande\SophosLogs;

class SophosLogFilePool
{
    private array $LogFilenames = [];
    private ?SophosLogLineProcessor $Processor = null;
    private array $Aggregators = [];
    private array $IncludeFilters = [];
    private array $ExcludeFilters = [];

    public function __construct(array $TheLogFilenames)
    {
        $this->LogFilenames = $TheLogFilenames;
    }

    public function addToExcludeFilters(string $AText): void
    {
        $this->ExcludeFilters[] = $AText;
    }

    public function addToIncludeFilters(string $AText): void
    {
        $this->IncludeFilters[] = $AText;
    }

    public function addAggregator(\RioGrande\Logs\LogAggregator $Aggregator): void
    {
        $this->Aggregators[] = $Aggregator;
    }

    public function getAggregators(): array
    {
        return $this->Aggregators;
    }

    public function setProcessor(SophosLogLineProcessor $AProcessor): void
    {
        $this->Processor = $AProcessor;
    }

    public function getProcessed(): string
    {
        $sRet = (isset($this->Processor)) ? $this->Processor->getFilePrefix() : '';
        $i = 0;
        foreach ($this->LogFilenames as $sFilename) {
            $f = new SophosLogFile($sFilename);
            try {
                if (isset($this->Processor)) {
                    $f->setProcessor($this->Processor);
                }
                $f->setAggregators($this->Aggregators);
                $f->setExcludeFilters($this->ExcludeFilters);
                $f->setIncludeFilters($this->IncludeFilters);
                $sRet .= $f->getProcessed();
            } finally {
                unset($f);
            }
            $i++;
        }
        $sRet .= $this->Processor->getFileSuffix();
        return $sRet;
    }
}
