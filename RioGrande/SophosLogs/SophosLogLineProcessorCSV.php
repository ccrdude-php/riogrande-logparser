<?php

/**
 * Processes data from a single log line into CSV output.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage SophosLogs
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-logparser
 * @since      0.1.0
 */

namespace RioGrande\SophosLogs;

class SophosLogLineProcessorCSV extends SophosLogLineProcessor
{
    public function getFilePrefix(): string
    {
        $sFields = implode(';', $this->Fields);
        return "{$sFields}\n";
    }

    public function getFileSuffix(): string
    {
        return "";
    }

    public function getTextFromLine(SophosLogLine $ALine): string
    {
        $aData = $this->getDataArray($ALine);
        $aOut = array();
        foreach ($this->Fields as $sField) {
            $aOut[] = '"' . $aData[$sField] . '"';
        }
        $sRet = implode(';', $aOut);
        $sRet .= "\n";
        return $sRet;
    }
}
