<?php

/**
 * Processes data from a single log line.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage SophosLogs
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-logparser
 * @since      0.1.0
 */

namespace RioGrande\SophosLogs;

abstract class SophosLogLineProcessor
{
    abstract public function getFilePrefix(): string;
    abstract public function getFileSuffix(): string;
    abstract public function getTextFromLine(SophosLogLine $ALine): string;

    protected array $QueryFields = [];
    protected array $URLFields = [];
    protected array $CookieFields = [];
    protected array $Fields = ['date', 'time', 'ip', 'statuscode', 'timecode', 'url', 'query', 'cookie'];

    public function addQueryField(string $AQueryID, string $AFieldID, bool $IsBasde64Encoded = false): void
    {
        $this->QueryFields[] = (object)array('field' => $AFieldID, 'id' => $AQueryID, 'base64' => $IsBasde64Encoded);
        $this->Fields[] = $AFieldID;
    }

    public function addURLField(string $AFieldID, string $ARegularExpression): void
    {
        $this->URLFields[] = (object)array('field' => $AFieldID, 'regexp' => $ARegularExpression);
        $this->Fields[] = $AFieldID;
    }

    public function addCookieField(string $AFieldID, string $ARegularExpression): void
    {
        $this->CookieFields[] = (object)array('field' => $AFieldID, 'regexp' => $ARegularExpression);
        $this->Fields[] = $AFieldID;
    }

    public function getDataArray(SophosLogLine $ALine): array
    {
        $aData = array();
        foreach ($this->Fields as $sField) {
            $aData[$sField] = '';
        }
        $aData['date'] = $ALine->getDate();
        $aData['time'] = $ALine->getTime();
        $aData['ip'] = $ALine->getIP();
        $aData['statuscode'] = $ALine->getStatusCode();
        $aData['timecode'] = $ALine->getTimeCode();
        $aData['url'] = $ALine->getURL();
        $aData['query'] = $ALine->getQuery();
        $aData['cookie'] = $ALine->getCookie();
        $aData = $this->processFields($aData);
        return $aData;
    }

    protected function processFields(array $AData): array
    {
        $ret = $AData;

        foreach ($this->URLFields as $oField) {
            $sExpression = $oField->regexp;
            preg_match_all($sExpression, $AData['url'], $matches, PREG_SET_ORDER, 0);
            if ((!is_null($matches)) && (count($matches) > 0)) {
                $ret[$oField->field] = "{$matches[0][1]}";
            }
        }

        $sQuery = $AData['query'];
        if (0 == strpos($sQuery, '?')) {
            $sQuery = substr($sQuery, 1);
        }
        parse_str($sQuery, $aQueryParts);
        foreach ($this->QueryFields as $oField) {
            if (isset($aQueryParts[$oField->id])) {
                $sData = $aQueryParts[$oField->id];
                if ($oField->base64) {
                    $sData = base64_decode($sData);
                    $sData = substr($sData, 0, 46);
                }
                $ret[$oField->field] = $sData;
            }
        }

        foreach ($this->CookieFields as $oField) {
            $sExpression = $oField->regexp;
            preg_match_all($sExpression, $AData['cookie'], $matches, PREG_SET_ORDER, 0);
            if ((!is_null($matches)) && (count($matches) > 0)) {
                $ret[$oField->field] = "{$matches[0][1]}";
            }
        }
        return $ret;
    }
}
