<?php

/**
 * Handles a Sophos Firewall log file line.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage SophosLogs
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-logparser
 * @since      0.1.0
 */

namespace RioGrande\SophosLogs;

/**
 * This class parses essential information from Sophos Firewall log lines.
 *
 * @package Defiant\SophosLogs
 */
class SophosLogLine extends \RioGrande\Logs\LogLine
{
    private string $Line = '';
    //private string $RegularExpression = '/\.\/reverseproxy-([0-9]{4}-[0-9]{2}-[0-9]{2})\.log:([0-9]{4}:[0-9]{2}:[0-9]{2})-([0-9]{2}:[0-9]{2}:[0-9]{2}).*srcip="(\d+\.\d+\.\d+\.\d+)".*statuscode="(\d+)".*time="(\d+)".*url="([^\"]*)".*query="([^\"]*)"/m';
    private string $RegularExpression = '/[^\:]*:' // just the grep prefix
        . '([0-9]{4}):([0-9]{2}):([0-9]{2})-([0-9]{2}:[0-9]{2}:[0-9]{2}).*' // date and time
        . 'srcip="(\d+\.\d+\.\d+\.\d+)".*' // source IP
        . 'statuscode="(\d+)".*' // status code
        . 'time="(\d+)".*' // time
        . 'url="([^\"]*)".*' // url
        . 'query="([^\"]*)".*' // query
        . 'cookie="([^\"]*)"?/m';
    private array $Matches = [];
    private int $IndexDate = 1;
    private int $IndexTime = 4;
    private int $IndexIP = 5;
    private int $IndexStatusCode = 6;
    private int $IndexTimeCode = 7;
    private int $IndexURL = 8;
    private int $IndexQuery = 9;
    private int $IndexCookie = 10;

    public function __construct(string $TheLine)
    {
        $this->Line = $TheLine;
        preg_match_all($this->RegularExpression, $TheLine, $this->Matches, PREG_SET_ORDER, 0);
    }

    public function hasResults(): bool
    {
        return (count($this->Matches) > 0);
    }

    public function getDate(): string
    {
        $sRet = $this->Matches[0][$this->IndexDate];
        $sRet .= '-' . $this->Matches[0][$this->IndexDate + 1];
        $sRet .= '-' . $this->Matches[0][$this->IndexDate + 2];
        return $sRet;
    }

    public function getTime(): string
    {
        return $this->Matches[0][$this->IndexTime];
    }

    public function getIP(): string
    {
        return $this->Matches[0][$this->IndexIP];
    }

    public function getStatusCode(): string
    {
        return $this->Matches[0][$this->IndexStatusCode];
    }

    public function getTimeCode(): string
    {
        return $this->Matches[0][$this->IndexTimeCode];
    }

    public function getURL(): string
    {
        return $this->Matches[0][$this->IndexURL];
    }

    public function getQuery(): string
    {
        return $this->Matches[0][$this->IndexQuery];
    }

    public function getCookie(): string
    {
        return trim($this->Matches[0][$this->IndexCookie]);
    }
}
