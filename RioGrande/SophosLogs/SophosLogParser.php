<?php

/**
 * Main log parsing handle class.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage SophosLogs
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-logparser
 * @since      0.1.0
 */

namespace RioGrande\SophosLogs;

class SophosLogParser
{
    private ?string $FileFormat = null;
    private string $InputListFilename = 'input.lst';
    private bool $ShowHelp = false;
    private bool $ListAttachments = false;
    private bool $ListCalendar = false;
    private bool $ListLogon = false;
    private bool $StatLogonDates = false;
    private bool $StatLogonCounts = false;
    private bool $DisplayStats = false;

    public function __construct()
    {
        $options = getopt(
            'hacl',
            ['format:', 'help', 'attachments', 'ics', 'calendar', 'logon', 'input:', 'statistics:']
        );
        $this->ShowHelp = (isset($options['h']) || (isset($options['help'])));
        if (isset($options['format'])) {
            $this->FileFormat = $options['format'];
        }
        if (isset($options['input'])) {
            $this->InputListFilename = $options['input'];
        }
        $this->ListAttachments = (isset($options['a']) || (isset($options['attachments'])));
        $this->ListCalendar = (isset($options['c']) || (isset($options['ics'])));
        $this->ListLogon = (isset($options['l']) || (isset($options['logon'])));
        $aStats = isset($options['statistics']) ? explode(',', $options['statistics']) : [];
        $this->StatLogonDates = in_array('logondates', $aStats);
        $this->StatLogonCounts = in_array('logoncounts', $aStats);
        $this->DisplayStats = $this->StatLogonDates || $this->StatLogonCounts;
    }

    protected function getConsoleHeader(): string
    {
        return <<<FOOBAR
        SophosLog Line Parser 0.1
        (c) 2024 Patrick Kolla-ten Venne. All rights reserved.
        Not associated with Sophos in any way.

        
        FOOBAR;
    }

    protected function getHelpText(): string
    {
        return <<<FOOBAR
        Input needs to be a grep result (prefixed by source locations)

        Possible options:

          -h, --help   This screen.

          --input=filename    Specify an input file listing all logs to check

          --format=csv        Print out all files as CSV.
          --format=json       Print out all files as JSON.

        The following filters can be combined.
        If none is specified, all lines will be converted.

          -a  --attachments   Display lines with GetFileAttachment
          -c  --ics           Display lines with calendar queries
          -l  --logon         Display lines with logon attempts

          --statistics=logoncounts    Displays logon counts per username
          --statistics=logondates     Displays logon counts per username per date


        FOOBAR;
    }

    protected function addFilters(SophosLogFilePool $Pool, SophosLogLineProcessor $Processor): void
    {
        if ($this->ListAttachments) {
            $Processor->addQueryField('id', 'attachmentid', false);
            $Processor->addQueryField('X-OWA-CANARY', 'owacanary', false);
            $Pool->addToIncludeFilters('/owa/service.svc/s/GetFileAttachment');
        } elseif ($this->ListCalendar) {
            $Processor->addURLField('account', '/\/owa\/calendar\/([0-9a-f]*@[^\/]*)\/[0-9a-f]*\/calendar.ics/');
            $Processor->addURLField('id', '/\/owa\/calendar\/[0-9a-f]*@[^\/]*\/([0-9a-f]*)\/calendar.ics/');
            $Pool->addToIncludeFilters('/owa/calendar/');
        } elseif ($this->ListLogon) {
            $Processor->addCookieField('username', '/lgn=([a-z\.]*@[^\;]*)/');
            $Pool->addToIncludeFilters('url="/owa/auth/logon.aspx');
        }
    }

    public function execute(): void
    {
        if ($this->ShowHelp) {
            echo $this->getConsoleHeader();
            die($this->getHelpText());
        }
        if (null == $this->FileFormat) {
            echo $this->getConsoleHeader();
            die("Please specify --format=(csv|json)\n");
        }
        if (!file_exists($this->InputListFilename)) {
            echo $this->getConsoleHeader();
            die("Please specify an existing file using --input=filename\n");
        }
        $aFilesIn = file($this->InputListFilename);
        $aFiles = array();
        foreach ($aFilesIn as $sFile) {
            $aFiles[] = trim($sFile);
        }

        $oAggregatorLogonCounts = null;
        $oAggregatorLogonDates = null;

        $fp = new SophosLogFilePool($aFiles);
        if ($this->StatLogonDates) {
            $oAggregatorLogonDates = new \RioGrande\Logs\LogAggregatorLogonDates();
            $fp->addAggregator($oAggregatorLogonDates);
        }
        if ($this->StatLogonCounts) {
            $oAggregatorLogonCounts = new \RioGrande\Logs\LogAggregatorLogonCounts();
            $fp->addAggregator($oAggregatorLogonCounts);
        }
        switch ($this->FileFormat) {
            case 'csv':
                $p = new SophosLogLineProcessorCSV();
                $fp->setProcessor($p);
                $this->addFilters($fp, $p);
                $s = $fp->getProcessed();
                if (!$this->DisplayStats) {
                    print($s);
                }
                break;
            case 'json':
                $p = new SophosLogLineProcessorJSON();
                $fp->setProcessor($p);
                $this->addFilters($fp, $p);
                $s = $fp->getProcessed();
                if (!$this->DisplayStats) {
                    print($s);
                }
                break;
            default:
                $p = new SophosLogLineProcessorSilent();
                $fp->setProcessor($p);
                $this->addFilters($fp, $p);
                $s = $fp->getProcessed();
                if (!$this->DisplayStats) {
                    print($s);
                }
                break;
        }

        foreach ($fp->getAggregators() as $agg) {
            print($agg->getFormattedData($this->FileFormat));
        }
    }
}
