<?php

/**
 * Fakes to process data from a single log line.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage SophosLogs
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-logparser
 * @since      0.1.0
 */

namespace RioGrande\SophosLogs;

class SophosLogLineProcessorSilent extends SophosLogLineProcessor
{
    public function getFilePrefix(): string
    {
        return "";
    }

    public function getFileSuffix(): string
    {
        return "";
    }

    public function getTextFromLine(SophosLogLine $ALine): string
    {
        return '';
    }
}
