<?php

/**
 * Handles a Sophos Firewall log file.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage SophosLogs
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-logparser
 * @since      0.1.0
 */

namespace RioGrande\SophosLogs;

/**
 * This class parses a complete Sophos Firewall log file.
 *
 * @package Defiant\SophosLogs
 */
class SophosLogFile
{
    private string $Filename;
    private ?SophosLogLineProcessor $Processor = null;
    private array $Aggregators = [];
    private array $IncludeFilters = [];
    private array $ExcludeFilters = [];

    public function __construct(string $TheFilename)
    {
        $this->Filename = $TheFilename;
    }

    public function addAggregator(\RioGrande\Logs\LogAggregator $Aggregator): void
    {
        $this->Aggregators[] = $Aggregator;
    }

    public function addToExcludeFilters(string $AText): void
    {
        $this->ExcludeFilters[] = $AText;
    }

    public function addToIncludeFilters(string $AText): void
    {
        $this->IncludeFilters[] = $AText;
    }

    public function setExcludeFilters(array $AFilters): void
    {
        $this->ExcludeFilters = $AFilters;
    }

    public function setIncludeFilters(array $AFilters): void
    {
        $this->IncludeFilters = $AFilters;
    }

    public function setAggregators(array $TheAggregators): void
    {
        $this->Aggregators = $TheAggregators;
    }

    public function setProcessor(SophosLogLineProcessor $AProcessor): void
    {
        $this->Processor = $AProcessor;
    }

    private function getLineIterator($AFileHandle)
    {
        while (!feof($AFileHandle)) {
            yield fgets($AFileHandle);
        }
    }

    private function testAddLine(string $TheLine): bool
    {
        if (count($this->IncludeFilters) > 0) {
            foreach ($this->IncludeFilters as $sFilter) {
                if (strpos($TheLine, $sFilter) !== false) {
                    return true;
                }
            }
            return false;
        } elseif (count($this->ExcludeFilters) > 0) {
            foreach ($this->ExcludeFilters as $sFilter) {
                if (strpos($TheLine, $sFilter) !== true) {
                    return false;
                }
            }
            return true;
        }
        return true;
    }

    public function getProcessed(): string
    {
        $sRet = '';
        $rFileHandle = fopen($this->Filename, 'r');
        try {
            $iLine = 0;
            foreach ($this->getLineIterator($rFileHandle) as $sLine) {
                $oLine = new SophosLogLine($sLine);
                try {
                    if (($oLine->hasResults()) && ($this->testAddLine($sLine))) {
                        // $sRet .= $iLine . ': ' . $sLine;
                        $sRet .= $this->Processor->getTextFromLine($oLine);
                        // $sRet .= "\n\n";
                        $aData = $this->Processor->getDataArray($oLine);
                        foreach ($this->Aggregators as $agg) {
                            $agg->processData($oLine, $aData);
                        }
                    }
                } finally {
                    unset($oLine);
                }
                $iLine++;
            }
        } finally {
            fclose($rFileHandle);
        }
        return $sRet;
    }
}
