# Sophos Firewall Log File Parser

This tool was created to extract useful data from Sophos Firewall data.

As input, it takes pre-filtered data, e.g. the file `results-8-8-8-8.log` from a grep query like this:

```
grep "8\.8\.8\.8" -R logfiles/ > results-8-8-8-8.log 
```

You can call `parselog.php --help` to receive details.

A simple output of the log file content as `.csv`, to be opened by a Spreadsheet application, can be run this way:

```
php ./parser/parselog.php --format=csv --input=files.lst
```

The file `files.lst` should name the files to search.