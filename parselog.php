<?php

/**
 * Command line tool to process logs specified by command line parameters.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage Logs
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-logparser
 * @since      0.1.0
 */

namespace RioGrande\SophosLogs;

spl_autoload_register(function ($ClassName) {
    $ClassName = str_replace('\\', '/', $ClassName);
    require_once $ClassName . '.php';
});


(new SophosLogParser())->execute();
